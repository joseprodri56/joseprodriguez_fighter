Made by Josep Rodríguez
Antes quiero comentar unas cuantas cosas.
No he puesto una barra de vida a mostrar por falta de tiempo, me he dedicado mas tiempo a bugs que tenia en el juego que eran desastrosos (si no fuesen desastrosos no les dedicaria tiempo, de hecho tengo bugs que no afectan tanto el juego).
La vida solo se mostrara en la consola con un Debug.Log()
Y solo podras controlar a luigi, mario es un simple cubo rojo que tira bolas de fuego y hace puñetazos cuando le sale un cubo amarillo (que esa es su hitbox).
Ampliaciones:
He puesto musica, solo musica. Nada de SFX.
Luigi tiene sprites, aparte que tu ya comentaste que no hace falta que sea bonito. Asi que las animaciones no estan perfectas, obviamente porque es una perdida de tiempo.
Quiero recalcar que la falta de tiempo es mas por bugs rompibles que las animaciones, las animaciones es lo que menos tiempo le he dedicado.
He puesto unas particulas en las bolas, no son perfectas, pero almenos da su toque.
Cuando te caes, al aterrizar al suelo reproducen unas particulas de impacto como si fuese el smash.
Cuando golpeo a alguien, reproducen otras particulas, tampoco son perfectas, pero tambien dan su toque.
He hecho un menu simple para escoger escena, battlefield y Omega, donde battlefield contiene las plataformas que puedes subir.
Como no queria hacer un smash (aunque me copie los escenarios de alli), pues he puesto un limite que te hace impedir salir del mapa. Es decir, si vas demasiado atras, un muro invisible (que mas bien lo he hecho por comandos) no te dejara pasar de mas.
Dijiste de hacer combos, pero no de que, asi que añadi tambien combos de movimiento en el cual si pulsas 2 veces una direccion, el personaje hace como un esquive (aunque recibiras daño).

/**
     * Controles
     * Flecha izquierda: 1 vez, moverse a la izquierda, 2 veces, esquivar a la izquierda (haces un saltito a la izquierda y vas mas lejos que al moverte)
     * Flecha derecha: 1 vez, moverse a la derecha, 2 veces, esquivar a la derecha (haces un saltito a la derechas y vas mas lejos que al moverte)
     * Flecha de arriba: saltar (tiene doble salto).
     * X: puñetazos, solo tiene 2 combos.
     * C: Tirar bolas de fuego. Tiene cooldown, pero puedes tirarlos tanto saltando que tirar puñetazos... Eb cualquier estado (no me he molestado en ponerle restriccion a eso).
     * Nota: hay un tercer combo que implemente en los controles, pero no le he puesto nada. Puedes decidir si puntuar esa parte del tercer combo o no.
**/