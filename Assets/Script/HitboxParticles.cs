﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitboxParticles : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<ParticleSystem>().Play();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (this.GetComponent<BoxCollider2D>().enabled){
            Debug.Log(this.GetComponent<BoxCollider2D>().enabled);
            this.GetComponent<ParticleSystem>().Play();
        }
        
    }
}
