﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class changeScene : MonoBehaviour
{
    //Estos metodos seran llamados por un solo gameobject triste en el cual solo recibe funciones de los botones del menu. Nota: se tienen que añadir las escenas desde File/Build Settings, si no, dara error
    public void BattleFieldScene()
    {
        SceneManager.LoadScene("BattleField");
    }

    public void OmegaScene()
    {
        SceneManager.LoadScene("Omega");
    }
}
