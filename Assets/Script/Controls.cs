﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controls : MonoBehaviour
{


    /**
     * Controles
     * Flecha izquierda: 1 vez, moverse a la izquierda, 2 veces, esquivar a la izquierda (haces un saltito a la izquierda y vas mas lejos que al moverte)
     * Flecha derecha: 1 vez, moverse a la derecha, 2 veces, esquivar a la derecha (haces un saltito a la derechas y vas mas lejos que al moverte)
     * Flecha de arriba: saltar (tiene doble salto).
     * X: puñetazos, solo tiene 2 combos.
     * C: Tirar bolas de fuego. Tiene cooldown, pero puedes tirarlos tanto saltando que tirar puñetazos... Eb cualquier estado (no me he molestado en ponerle restriccion a eso).
     * Nota: hay un tercer combo que implemente en los controles, pero no le he puesto nada. Puedes decidir si puntuar esa parte del tercer combo o no.
     **/






    //Lista de eventos
    //i: 0 left, 1 right; d: dodge true or false

    public delegate void Movement(int i, bool d);
    public delegate void Combo(int n_combo, String key);
    public event Movement MovementEvent;
    public event Combo ComboEvent;
    //El doSomething lo uso para comprobar que los subscriptores tienen conexion con los eventos o no (Como un hello world)
    //public event EventHandler doSomething;
    public event EventHandler jump;
    public event EventHandler fire;


    //Lo usare para los combos
    int comboStatus;
    int comboSpeedL = 0;
    int comboSpeedR = 0;
    // Update is called once per frame
    void Update()
    {

        inputs();

    }


    private void inputs()
    {

        if (Input.GetKeyDown("right"))
        {

            StartCoroutine(movecombo("right"));
        }
        if (Input.GetKey("right") && comboSpeedR !=2 && comboSpeedL != 2)
        {
            //El invoke servira en caso de que no haiga ningun subscriptor
            MovementEvent?.Invoke(1, false);
        }
        if (Input.GetKeyDown("left"))
        {
            StartCoroutine(movecombo("left"));
        }
        if (Input.GetKey("left") && comboSpeedR != 2 && comboSpeedL != 2)
        {
            MovementEvent?.Invoke(0, false);
        }
        if (Input.GetKeyDown("up"))
        {
            jump.Invoke(this, EventArgs.Empty);
        }
        if(Input.GetKeyDown("c")){
            //StartCoroutine(fireballcooldown());
            fire?.Invoke(this, EventArgs.Empty);

        }
        if (Input.GetKeyDown("x"))
        {
            StartCoroutine(comboList("x"));
            

        }

    }

    private IEnumerator comboList(String inp)
    {
        if (comboStatus == 0)
        {
            Debug.Log("Doing Combo 0");
            ComboEvent?.Invoke(0, inp);
            comboStatus++;
            yield return new WaitForSeconds(0.25f);
            //Si sigue siendo 1, vuelve a 0
            if (comboStatus == 1)
            {
                Debug.Log("You end in 0");
                comboStatus=0;
            }
        }
        else if(comboStatus == 1)
        {
            Debug.Log("Doing Combo 1");
            ComboEvent?.Invoke(1, inp);
            comboStatus++;
            yield return new WaitForSeconds(0.25f);
            if (comboStatus == 2)
            {
                Debug.Log("You end in 1");
                comboStatus = 0;
                ComboEvent?.Invoke(0, "");
            }
        }
        else if(comboStatus == 2)
        {
            Debug.Log("Doing Combo 2");
            ComboEvent?.Invoke(2, inp);
            comboStatus++;
            yield return new WaitForSeconds(0.25f);
            Debug.Log("Returning combo in 0");
            //Fin del trayecto;
            comboStatus = 0;
            ComboEvent?.Invoke(0, "");
        }
    }





    IEnumerator movecombo(string direction)
    {
        if (direction.Equals("right"))
        {
            if (comboSpeedR == 0 && comboSpeedL==0)
            {
                //Si se ejecuta por primera vez, el int pasa a 1, si despues de x segundos sigue estando en 1, el int se pasa a 0;
                comboSpeedR = 1;
                yield return new WaitForSeconds(0.25f);
                if (comboSpeedR == 1)
                {
                    comboSpeedR = 0;
                }
                
            }
            else if(comboSpeedR == 1)
            {
                //si el comboSpeedR es igual a 1 y ha entrado antes de que vuelva a 0 en el if anterior, el int se pasa a 2, ejecuta el esquive, y dentro de unos segundos se pasa a 0
                comboSpeedR = 2;
                MovementEvent?.Invoke(1, true);
                //dodgeright();
                yield return new WaitForSeconds(0.35f);
                comboSpeedR = 0;
            }
            
        }
        else if (direction.Equals("left"))
        {
            if (comboSpeedL == 0 && comboSpeedR == 0)
            {
                comboSpeedL = 1;
                yield return new WaitForSeconds(0.25f);
                if (comboSpeedL == 1)
                {
                    comboSpeedL = 0;
                }

            }
            else if (comboSpeedL == 1)
            {
                comboSpeedL = 2;
                MovementEvent?.Invoke(0, true);
                //dodgeleft();
                yield return new WaitForSeconds(0.35f);
                comboSpeedL = 0;
            }

        }

    }


    


}
