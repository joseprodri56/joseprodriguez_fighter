﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Balls : MonoBehaviour
{

    void FixedUpdate()
    {
        if (this.transform.position.x <= -13)
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Si tiene uno de esos tags (que es la hurtbox de mario o luigi y las bolas de fuego), entra y se destruye la bola de fuego correspondiente.
        if(collision.gameObject.tag =="Mario" || collision.gameObject.tag == "Luigi")
        {
            Destroy(gameObject);
        }
    }
}
