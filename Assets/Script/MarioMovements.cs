﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MarioMovements : MonoBehaviour
{

    private int life = 200;
    GameObject luigi;
    GameObject MarioHB;
    Collider2D myHitBox;
    SpriteRenderer HitBoxRenderer;
    public GameObject fireBall;
    private bool outofFuel= false;
    //A diferencia de luigi, este se pone true porque mario esta en la derecha mirando a la izquierda a luigi.
    static bool direction = true;
    bool nopunching= true;

    void Start()
    {
        luigi = GameObject.Find("Luigi");
        MarioHB = GameObject.Find("HitboxM");
        myHitBox = MarioHB.GetComponent<Collider2D>();
        HitBoxRenderer = MarioHB.GetComponent<SpriteRenderer>();

    }

    

    void Update()
    {
        if (life > 0)
        {
            
            Debug.Log("Mario " + life);
        }
        else
        {
            SceneManager.LoadScene("Menu");
            Debug.Log("You are ded, Mario");
        }
        
        positionenemy();
        StartCoroutine(fireballcooldown());
        StartCoroutine(punching());
    }

    private IEnumerator punching()
    {
        if (nopunching)
        {
            nopunching = false;
            myHitBox.enabled = !myHitBox.enabled;
            HitBoxRenderer.enabled = !HitBoxRenderer.enabled;
            yield return new WaitForSeconds(3f);
            nopunching = true;
        }
        
    }

    private IEnumerator fireballcooldown()
    {
        if (!outofFuel)
        {
            outofFuel = true;
            fireball();
            yield return new WaitForSeconds(6f);
            Debug.Log(true);
            outofFuel = false;
        }
    }

    private void fireball()
    {
        GameObject fireBallShoot = Instantiate(fireBall);
        //False es mirando a la derecha
        if (!direction)
        {
            fireBallShoot.transform.position = new Vector2(transform.position.x + 1.5f, transform.position.y);
            //fireBallShoot.GetComponent<Rigidbody2D>().AddForce(transform.forward * 900f);
            fireBallShoot.GetComponent<Rigidbody2D>().velocity = new Vector2(5f, fireBallShoot.GetComponent<Rigidbody2D>().velocity.y);
        }
        else
        {
            fireBallShoot.transform.position = new Vector2(transform.position.x - 1.5f, transform.position.y);
            fireBallShoot.GetComponent<Rigidbody2D>().velocity = new Vector2(-5f, fireBallShoot.GetComponent<Rigidbody2D>().velocity.y);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //&& gameObject.name != "MarioPuppet" 
        if ((collision.gameObject.tag == "Luigi" && collision.gameObject.name == "Hitbox"))
        {
            Debug.Log(collision.gameObject.name);
            Debug.Log(gameObject.tag);
            Debug.Log(gameObject.name);
            hurt(direction);
            print("Mario recieve damage by "+collision.tag);
        }
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Luigi" && collision.gameObject.name == "GreenBall(Clone)")
        {
            //Por la inercia de la bola, la bola misma empuja al personaje y como le asigno daño, pues el impacto del daño + la inercia, se alejaba el doble.
            //Asi que le pongo antes un 0 en su x para que cuando colisione, solo reaccione al impacto del daño y se aleje segun su daño y no segun la velocidad de la bola
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
            hurt(direction);
            //Debug.Log(true);
        }
    }

    private void hurt(bool direction)
    {
        life -= 10;
        float force = 125f;
        //Mirando a la derecha
        if (!direction)
        {
            this.gameObject.GetComponent<Rigidbody2D>().AddForce(transform.right * (-force));
            this.gameObject.GetComponent<Rigidbody2D>().AddForce(transform.up * force);
        }
        //Mirando a la izquierda
        else
        {
            this.gameObject.GetComponent<Rigidbody2D>().AddForce(transform.right * force);
            this.gameObject.GetComponent<Rigidbody2D>().AddForce(transform.up * force);
        }
    }

    private void positionenemy()
    {
        if (luigi.transform.position.x > this.transform.position.x)
        {
            direction = false;
        }
        else if (luigi.transform.position.x < this.transform.position.x)
        {
            direction = true;
        }
    }

}
